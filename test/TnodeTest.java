
import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author Jaanus
 */
public class TnodeTest {

   @Test (timeout=1000)
   public void testBuildFromRPN() { 
      String s = "1 2 +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(1,2)", r);
      s = "2 1 - 4 * 6 3 / +";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(6,3))", r);
   }

   @Test (timeout=1000)
   public void testBuild2() {
      String s = "512 1 - 4 * -61 3 / +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-61,3))", r);
      s = "5";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "5", r);
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol() {
      Tnode t = Tnode.buildFromRPN ("2 xx");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol2() {
      Tnode t = Tnode.buildFromRPN ("x");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol3() {
      Tnode t = Tnode.buildFromRPN ("2 1 + xx");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers() {
      Tnode root = Tnode.buildFromRPN ("2 3");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers2() {
      Tnode root = Tnode.buildFromRPN ("2 3 + 5");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers() {
      Tnode t = Tnode.buildFromRPN ("2 -");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers2() {
      Tnode t = Tnode.buildFromRPN ("2 5 + -");
   }

  @Test (expected=RuntimeException.class)
   public void testTooFewNumbers3() {
      Tnode t = Tnode.buildFromRPN ("+");
   }

   @Test (timeout=1000)
   public void testAdditional() {
      String base1 = "2 5 SWAP -";
      Tnode first = Tnode.buildFromRPN (base1);
      String firstStr = first.toString();
      assertEquals ("Tree: " + first, "-(5,2)", firstStr);

      String base2 = "3 DUP *";
      Tnode second = Tnode.buildFromRPN (base2);
      String secondStr = second.toString();
      assertEquals ("Tree: " + second, "*(3,3)", secondStr);

      String base3 = "2 5 9 ROT - +";
      Tnode third = Tnode.buildFromRPN (base3);
      String thirdStr = third.toString();
      assertEquals ("Tree: " + third, "+(5,-(9,2))", thirdStr);

      String base4 = "2 5 9 ROT + SWAP -";
      Tnode fourth = Tnode.buildFromRPN (base4);
      String fourthStr = fourth.toString();
      assertEquals ("Tree: " + fourth, "-(+(9,2),5)", fourthStr);

      String base5 = "2 5 DUP ROT - + DUP *";
      Tnode fifth = Tnode.buildFromRPN (base5);
      String fifthStr = fifth.toString();
      assertEquals ("Tree: " + fifth, "*(+(5,-(5,2)),+(5,-(5,2)))", fifthStr);

      String base6 = "-3 -5 -7 ROT - SWAP DUP * +";
      Tnode sixth = Tnode.buildFromRPN (base6);
      String sixthStr = sixth.toString();
      assertEquals ("Tree: " + sixth, "+(-(-7,-3),*(-5,-5))", sixthStr);
   }

   @Test (expected=RuntimeException.class)
   public void testDUP() {
      Tnode t = Tnode.buildFromRPN ("DUP");
   }

   @Test (expected=RuntimeException.class)
   public void testSWAP() {
      Tnode t = Tnode.buildFromRPN ("5 SWAP");
   }

   @Test (expected=RuntimeException.class)
   public void testROT() {
      Tnode t = Tnode.buildFromRPN ("5 9 ROT");
   }

}

