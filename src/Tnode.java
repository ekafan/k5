import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private static final LinkedList<String> correctOp =
           new LinkedList<>(Arrays.asList("+", "-", "*", "/", "DUP", "SWAP", "ROT"));

   @Override
   public String toString() {

      if (firstChild == null && nextSibling == null) {
         return name;
      }

      return new StringBuffer().append(name)
              .append("(").append(firstChild)
              .append(",").append(nextSibling)
              .append(")").toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Tnode root = new Tnode();

      ArrayList<String> elements = new ArrayList<>(Arrays.asList(pol.split(" ")));

      root.name = elements.get(0);

       findingException(elements, pol);

       if (elements.size() == 1) {
           root.name = elements.get(0);
          return root;
      }

      LinkedList<Tnode> opList = new LinkedList<>();

      for (String element : elements) {

          try {
              Integer.parseInt(element);
              Tnode newNum = buildFromRPN(element);
              opList.push(newNum);

          } catch (NumberFormatException e) {

              Tnode op = opList.pop();

              if (element.equals("DUP")) {
                  opList.push(op);
                  opList.push(op);
                  continue;
              }

              if (opList.size() < 1) {
                  throw new RuntimeException("Expression " + pol + " has not enough numbers for " + element + " operation");
              }

              if (element.equals("SWAP")) {
                  Tnode op2 = opList.pop();
                  opList.push(op);
                  opList.push(op2);
                  continue;

              } else if (element.equals("ROT")) {
                  if (opList.size() < 2) {
                      throw new RuntimeException("Expression " + pol + " has not enough numbers for ROT operation");
                  }

                  Tnode op2 = opList.pop();
                  Tnode op3 = opList.pop();

                  opList.push(op2);
                  opList.push(op);
                  opList.push(op3);
                  continue;
              }

              Tnode op1 = opList.pop();
              Tnode newOp = new Tnode();
              newOp.name = element;
              newOp.firstChild = op1;
              newOp.nextSibling = op;
              opList.push(newOp);
          }
      }
    return opList.pop();
   }

   private static void findingException(ArrayList<String> list, String pol) {
       if (list.size() == 1) {
           try {
               Integer.parseInt(list.get(0));
           } catch (NumberFormatException e) {
               throw new RuntimeException("Invalid element " + list.get(0) + " in current expression : " + pol);
           }
           return;
       }

      for (String str : list) {
         try {
            Integer.parseInt(str);
         } catch (NumberFormatException e) {
            if (!correctOp.contains(str)) {
               throw new RuntimeException("Invalid element " + str + " in current expression : " + pol);
            }
         }
      }

      if (!correctOp.contains(list.get(list.size()-1))) {
          throw new RuntimeException("Expression " + pol + " must end with operation, not number");
      }

      if (list.size() > 1) {
          try {
              Integer.parseInt(list.get(0));
          } catch (NumberFormatException e) {
              throw new RuntimeException("Expression " + pol + " has not enough numbers");
          }
      }
   }

   public static void main (String[] param) {
      String rpn = "1 2 +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res); // "+(1,2)"

      Tnode res1 = buildFromRPN("5");
      System.out.println("res1 : " + res1); // 5

      Tnode res2 = buildFromRPN("2 1 - 4 * 6 3 / +");
      System.out.println("res2 : " + res2); // +(*(-(2,1),4),/(6,3))

      Tnode res3 = buildFromRPN("5 1 + 2 / 3 * 4 +");
      System.out.println("res3 : " + res3); // +(*(/(+(5,1),2),3),4)

      Tnode res4 = buildFromRPN("7 5 - 1 8 + * 2 3 * 3 / +");
      System.out.println("res4 : " + res4); // +(*(-(7,5),+(1,8)),/(*(2,3),3))
   }
}
